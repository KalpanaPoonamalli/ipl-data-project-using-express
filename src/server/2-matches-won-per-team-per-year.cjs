function matchesWonPerTeamPerYear(matches){
   
    const matchesWonPerYears = matches.reduce((accumulator, currentValue)=>{
        
        if (accumulator[currentValue.season]){
            
            if (accumulator[currentValue.season][currentValue.winner]){

                accumulator[currentValue.season][currentValue.winner] ++;
            }else {
                accumulator[currentValue.season][currentValue.winner] = 1;
            }

        } else{

            accumulator[currentValue.season] = {}
            accumulator[currentValue.season][currentValue.winner] = 1;
        }

        return accumulator;
    }, {})


    return matchesWonPerYears
}

module.exports = matchesWonPerTeamPerYear;