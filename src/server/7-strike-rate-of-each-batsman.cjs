
function strikeRateForBatsman(matches, deliveries){

    const tables = matches.reduce((accumulator, currentValue) => {
        accumulator[currentValue.id] = currentValue.season
        return accumulator
    }, {});


    const strikeRateForBatsman = {};

    
   deliveries.map(delivery =>{


        let season = tables[delivery.match_id];
    

        if(strikeRateForBatsman[delivery.batsman]){
            if (strikeRateForBatsman[delivery.batsman][season]){
                strikeRateForBatsman[delivery.batsman][season].total_runs += parseInt(delivery.total_runs)
                strikeRateForBatsman[delivery.batsman][season].total_batsman += 1
                strikeRateForBatsman[delivery.batsman][season].strike += parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
            } else {
                strikeRateForBatsman[delivery.batsman][season] = {}
                strikeRateForBatsman[delivery.batsman][season].total_runs = parseInt(delivery.total_runs)
                strikeRateForBatsman[delivery.batsman][season].total_batsman = 1
                strikeRateForBatsman[delivery.batsman][season].strike = parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
            }
        } else{
            strikeRateForBatsman[delivery.batsman] = {}
            strikeRateForBatsman[delivery.batsman][season] = {}
            strikeRateForBatsman[delivery.batsman][season].total_runs = parseInt(delivery.total_runs)
            strikeRateForBatsman[delivery.batsman][season].total_batsman = 1
            strikeRateForBatsman[delivery.batsman][season].strike = parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
        }
            

        }) 

        // console.log(strikeRateForBatsman)
        return strikeRateForBatsman;
}

module.exports = strikeRateForBatsman;

