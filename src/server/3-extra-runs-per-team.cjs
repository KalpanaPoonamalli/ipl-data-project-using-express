
function extraRunsPerTeam( matches, deliveries){

    const year = 2016;
    const deliveriesDataIn2016 = deliveries.filter(delivery => {
        const match = matches.find(match => {
            return match.id === delivery.match_id
        });
        return match && match.season == year ;
    });

    const extraRunsConceded = {};

    deliveriesDataIn2016.map(delivery => {
        const bowlingTeam = delivery.bowling_team;
        const extraRuns = parseInt(delivery.extra_runs);

        if(extraRunsConceded[bowlingTeam]){
            extraRunsConceded[bowlingTeam] += extraRuns

        } else 
            extraRunsConceded[bowlingTeam] = extraRuns ;
    }) 

    return extraRunsConceded;

}

module.exports = extraRunsPerTeam;