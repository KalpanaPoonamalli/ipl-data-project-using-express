function topEconomialBowlers(matches, deliveries){
    const year = 2015;
    const deliveriesDataIn2015 = deliveries.filter(delivery => {
        const match = matches.find(match => match.id === delivery.match_id);
        return match && match.season == year 
    })

    const totalBalls = {};
    const totalRuns = {};

    deliveriesDataIn2015.map(delivery => {
        
        if(totalBalls[delivery.bowler]){
            totalBalls[delivery.bowler] += 1;
        } else {
            totalBalls[delivery.bowler] = 1;
        }

        if(totalRuns[delivery.bowler]){
            totalRuns[delivery.bowler] += parseInt(delivery.total_runs) 
        } else {
            totalRuns[delivery.bowler] = parseInt(delivery.total_runs) 
        }
    })
    

    const economyRates = {};

    Object.keys(totalBalls).map((bowler) => {
        const balls = totalBalls[bowler];
        const runs = totalRuns[bowler];
        const economyRate = (runs / balls) * 6;

        economyRates[bowler] = parseInt(economyRate);
    })

    const sortedBowlerEconomy = Object.entries(economyRates).sort((a,b) => {
        return a[1] - b[1]
    });


    return sortedBowlerEconomy.slice(0,10);
}

module.exports = topEconomialBowlers;