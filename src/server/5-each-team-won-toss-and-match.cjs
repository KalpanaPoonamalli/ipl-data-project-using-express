
function eachTeamWonTossAndMatch(matches){
    const tossAndMatchWinner = {};

    matches.map(match =>{
        if (match.toss_winner == match.winner){
            tossAndMatchWinner[match.toss_winner] += 1
        } else {
            tossAndMatchWinner[match.toss_winner] = 1;
        }
    })

    return tossAndMatchWinner;
}


module.exports = eachTeamWonTossAndMatch;