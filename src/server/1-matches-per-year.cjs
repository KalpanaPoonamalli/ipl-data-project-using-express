
function matchesPerYear(matches){

    const matchesPlayedPerYear = matches.reduce((accumulator, currentValue) => {
        if(accumulator[currentValue.season]){
            accumulator[currentValue.season] += 1;
        } else {
            accumulator[currentValue.season] = 1;
        }
        return accumulator
    }, {});

    // console.log(matchesPlayedPerYear)
    return matchesPlayedPerYear;
}




module.exports = matchesPerYear;
