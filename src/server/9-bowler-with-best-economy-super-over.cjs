
function bestEconomySuperOver(deliveries){

    const superOvers = {};

    deliveries.map(delivery => {
        if(superOvers[delivery.bowler]){
            superOvers[delivery.bowler] += parseInt(delivery.is_super_over);
        } else {
            superOvers[delivery.bowler] = parseInt(delivery.is_super_over);
        } 
    })
     

    const sortedBowlerEconomy = Object.entries(superOvers).sort((a,b) => b[1] - a[1]);

    return sortedBowlerEconomy.slice(0,1);
}

module.exports = bestEconomySuperOver;