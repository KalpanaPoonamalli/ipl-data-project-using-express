
function dismissedPlayer(deliveries){

    let players = {};

    deliveries.map(delivery => {

        if (delivery.player_dismissed){

            if (players[delivery.bowler]){

               if (players[delivery.bowler][delivery.player_dismissed]){
                    players[delivery.bowler][delivery.player_dismissed] += 1;
               } else{
                    players[delivery.bowler][delivery.player_dismissed] = 1;
               }

            } else{
                players[delivery.bowler] = {}
                players[delivery.bowler][delivery.player_dismissed] = 1;
            }
        }
    })


    const sortedPlayers = Object.entries(players).reduce((accumulator,[bowler, dismissedPlayer])=>{ 
        let add =Object.entries(dismissedPlayer).sort(([a1,b1],[a2,b2]) =>  {
            return b2 - b1 
        })[0]
        accumulator[bowler] = {
            "dismissed_player" : add[0],
            "dismissed_count" : add[1]
        } 
        return accumulator;
    }, {})


    return sortedPlayers;

}

module.exports = dismissedPlayer;