
function playerOfTheMatch(matches){

    const matchesForEachSeason = {};

    matches.map(match => {

        if (matchesForEachSeason[match.season]){

            if (matchesForEachSeason[match.season][match.player_of_match] ){
                matchesForEachSeason[match.season][match.player_of_match]++
            }else {
                matchesForEachSeason[match.season][match.player_of_match] = 1                
            }

        } else {

            matchesForEachSeason[match.season] = {}
            matchesForEachSeason[match.season][match.player_of_match] = 1  
        }
        
    })


    const sortedMatchesForEachSeason = Object.entries(matchesForEachSeason).reduce((accumulator, [years, values]) => {

        let s  =  Object.entries(values).sort((a, b) => {
            return b[1] - a[1]
        })[0]

        accumulator[years] = {
            "Batsman" : s[0],
            "Awards" : s[1]
        }
        
        return accumulator ;
        
    }, {})

    return sortedMatchesForEachSeason;

}

module.exports = playerOfTheMatch;