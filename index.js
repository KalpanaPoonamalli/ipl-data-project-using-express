const express = require("express");
const fs = require("fs");
const path = require("path");
const csvtojson = require("csvtojson");
const app = express();

const matchPath = path.join(__dirname, './src/data/matches.csv');
const deliveryPath = path.join(__dirname, './src/data/deliveries.csv');

const matchesPerYear = require('./src/server/1-matches-per-year.cjs'); 
const matchesWonPerYears = require('./src/server/2-matches-won-per-team-per-year.cjs'); 
const extraRunsPerTeam = require('./src/server/3-extra-runs-per-team.cjs'); 
const topEconomialBowlers = require('./src/server/4-top-economial-bowlers-in-year.cjs'); 
const eachTeamWonTossAndMatch = require('./src/server/5-each-team-won-toss-and-match.cjs'); 
const playerOfTheMatch = require('./src/server/6-player-of-the-match-award.cjs'); 
const strikeRateForBatsman = require('./src/server/7-strike-rate-of-each-batsman.cjs'); 
const dismissedPlayer = require('./src/server/8-one-player-dismissed-by-another-player.cjs'); 
const bestEconomySuperOver = require('./src/server/9-bowler-with-best-economy-super-over.cjs'); 


// 1

app.get('/matchesPerYear', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        const result = matchesPerYear(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        //console.log(`Error in converting the csv file to json file: ${err.message}`)
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 2

app.get('/matchesWonPerYears', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        const result = matchesWonPerYears(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 3

app.get('/extraRunsPerTeam', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        csvtojson()
        .fromFile(deliveryPath)
        .then((jsonObject1) => {
            const result = extraRunsPerTeam(jsonObject, jsonObject1);
            response.json(result);
        })
        .catch((err) => {
            response.status(500).json({
                Error: `Error in converting the csv file to json file: ${err.message}`
            })
        });
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 4


app.get('/topEconomialBowlers', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        csvtojson()
        .fromFile(deliveryPath)
        .then((jsonObject1) => {
            const result = topEconomialBowlers(jsonObject, jsonObject1);
            response.json(result);
        })
        .catch((err) => {
            response.status(500).json({
                Error: `Error in converting the csv file to json file: ${err.message}`
            })
        });
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 5

app.get('/eachTeamWonTossAndMatch', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        const result = eachTeamWonTossAndMatch(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})

// 6

app.get('/playerOfTheMatch', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        const result = playerOfTheMatch(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 7

app.get('/strikeRateForBatsman', (request, response) => {

    csvtojson()
    .fromFile(matchPath)
    .then((jsonObject) => {
        csvtojson()
        .fromFile(deliveryPath)
        .then((jsonObject1) => {
            const result = strikeRateForBatsman(jsonObject, jsonObject1);
            response.json(result);
        })
        .catch((err) => {
            response.status(500).json({
                Error: `Error in converting the csv file to json file: ${err.message}`
            })
        });
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 8


app.get('/dismissedPlayer', (request, response) => {

    csvtojson()
    .fromFile(deliveryPath)
    .then((jsonObject) => {
        const result = dismissedPlayer(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})


// 9


app.get('/bestEconomySuperOver', (request, response) => {

    csvtojson()
    .fromFile(deliveryPath)
    .then((jsonObject) => {
        const result = bestEconomySuperOver(jsonObject);
        response.json(result);
    })
    .catch((err) => {
        response.status(500).json({
            Error: `Error in converting the csv file to json file: ${err.message}`
        })
    });

})



const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});

